UUID_1 = "75c6ac9a-aca1-11eb-9e20-f875a4b810cc"
UUID_2 = "05781a4e-c17f-11eb-bd87-f875a4b810cc"
UUID_3 = "159b0508-c17f-11eb-85ab-f875a4b810cc"

UUID_PROPERTIES = {
    "durable": False,
    "auto_delete": True,
    "exclusive": False,
    "arguments": {},
}

QUEUES = {
    UUID_1: UUID_PROPERTIES,
    UUID_2: UUID_PROPERTIES,
    UUID_3: UUID_PROPERTIES,
}

BINDINGS = [
    {
        "exchange": "amq.topic",
        "queue": UUID_1,
        "routing_keys": [
            "org.centos.prod.ci.product-build.#",
            "org.centos.prod.ci.koji-build.#",
            "org.centos.prod.buildsys.#",
        ],
    },
    {
        "exchange": "amq.topic",
        "queue": UUID_2,
        "routing_keys": [
            "org.fedoraproject.prod.git.receive.#",
            "org.fedoraproject.prod.ci.koji-build.#",
        ],
    },
    {
        "exchange": "amq.topic",
        "queue": UUID_3,
        "routing_keys": [
            "org.centos.prod.buildsys.task.state.change",
        ],
    },
]
