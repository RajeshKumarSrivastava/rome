urls = {
    "cs9_manifest_url": (
        "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
        "main/package_list/cs9-image-manifest.json"
    ),
    "dover_submit_endpoint": "http://dover:8080/submit/package_list",
    # cs8 would get disabled in the atomotive-sig
    "c8s_manifest_url": (
        "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
        "main/package_list/c8s-image-manifest.txt"
    ),
    "cs8_manifest_url": (
        "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
        "main/package_list/cs8-image-manifest.json"
    ),
}
path = {"c8s_manifest_path": "mnt/data/c8s-image-manifest.txt"}
fileName = "cs9-image-manifest.json"
