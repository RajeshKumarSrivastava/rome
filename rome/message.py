import json
import logging
import os
import re
from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Any, Optional, Tuple

import fedora_messaging
import pkg_manifest_schema
import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder

import rpm_diff
from configs import app

logger = logging.getLogger("rome.Message")


class Message:
    LABELS = [
        "topic",
        "artifact",
        "status",
    ]

    TYPES = [
        "fedora-update",
        "koji-build",
        "koji-build-group",
        "product-build",
    ]

    def __init__(self):
        """
        Contructor for the Message class
        Default constructs
            - upstream_nvr
            - pkg_schema_obj
            - existing_nvr
        """
        self.upstream_nvr: str = ""
        self.pkg_schema_obj = pkg_manifest_schema.PackageManifestSchema()
        self.existing_nvr: str = ""

    def call_diff(self, current_nvr_list: list) -> Tuple[bool, list]:
        """
        The function calls the diff logic between the upstream_nvr and the
        the existing nvr
        Parameters
        ----------
        current_nvr_list: list, static list of pinned nvr

        Returns
        ------
        Tuple[bool, list]: bool for the success/failure
                           list is the new updated list in success,
                           unchanged list in failure
        """
        success = False
        for index, nvr in enumerate(current_nvr_list):
            nvr = nvr.strip()
            regex = r"-\d+"
            if (
                re.split(regex, self.upstream_nvr)[0]
                == re.split(regex, nvr)[0]
            ):
                self.existing_nvr = nvr
                if nvr == self.upstream_nvr:
                    break

                current_nvr_list[index] = rpm_diff.diff(nvr, self.upstream_nvr)
                success = True
                break
        return success, current_nvr_list

    def parse_and_diff(self) -> bool:
        """
        The function calls the call_diff function to perform the diff logic
        with all the lists of static pinned nvr in the repo file

        Returns
        ------
        bool: bool, for the success/failure depending on if a newer version of
        package was found

        """
        result: bool = False
        for k_, nvrs in vars(self.pkg_schema_obj).items():
            success, nvr_list = self.call_diff(nvrs)
            if success:
                nvrs = nvr_list
                result = True
                break

        return result

    def set_package_schema(self, url: str) -> bool:
        """
        The function calls get_upstream_manifest to get the remote file with
        static pinned list of nvr and sets the data in the schema class to
        retrieve and modify later

        Parameters
        ---------
        url: str, url of the file with the static list of pinned nvr
        """
        success: bool = False
        file_data: pkg_manifest_schema.schema = get_upstream_manifest(url)
        logger.debug(f"file_data: {file_data}")

        if file_data:
            self.pkg_schema_obj.deserialize(file_data)
            success = True
        else:
            logger.error("Trouble in getting file data")
        return success

    def stream_message(self, message: fedora_messaging) -> None:
        if not self.set_package_schema("cs9_manifest_url"):
            logger.error("Error in setting schema")
            return

        self.topic = message.topic
        self.artifact = {}
        self.status = ""

        if (
            "artifact" in message.body
            and message.body["artifact"]["type"] in self.TYPES
        ):
            try:
                self.upstream_nvr = message.body["artifact"].get(
                    "nvr"
                ) or message.body["artifact"]["builds"][0].get("nvr")

                self.artifact = {
                    "topic": message.body["artifact"]["type"],
                    "source": message.body["artifact"].get(
                        "source", "undefined"
                    ),
                }
            except KeyError as e:
                logger.exception(e)
            except Exception as e:
                logger.exception(e)

            result = self.parse_and_diff()

            if result:
                final_data = self.pkg_schema_obj.serialize()
                file_data_str: str = json.dumps(final_data)

                resp_code = submit_package(
                    self.existing_nvr,
                    self.upstream_nvr,
                    file_data_str,
                    "dover_submit_endpoint",
                    app.fileName,
                )
                self.status = resp_code
                self.artifact = {
                    "component": message.body["artifact"].get(
                        "component", "undefined"
                    )
                }

    def get_labels(self) -> Optional[dict]:
        """
        The function matches the attributes of class 'Message' to the
        corresponding labels.

        Returns
        ------
        dict: the detail of the incoming message.

        Example
        ------
        {"topic": "", "artifact": {"topic": "", "source": "", "component": ""},
         "status": ""}

        """
        try:
            return {
                label: self.__getattribute__(label) for label in self.LABELS
            }

        except (AttributeError, KeyError) as e:
            logger.exception(e)


def update_manifest_file(nvrs: list, manifest_file: str) -> int:
    try:
        Path(
            os.path.join(Path(__file__).parent.parent.absolute(), "mnt/data")
        ).mkdir(parents=True, exist_ok=True)
    except PermissionError as e:
        logger.exception(e)

    try:
        with open(
            os.path.join(
                Path(__file__).parent.parent.absolute(),
                app.path[manifest_file],
            ),
            "w+",
        ) as f:
            for nvr in nvrs:
                f.write(f"{nvr}\n")
        return 1

    except (KeyError, OSError) as e:
        logger.exception(e)
        return 0


def submit_package(
    nvr: str, upstream_nvr: str, file_data: str, endpoint: str, file_name: str
) -> int:
    """
    This function will send POST request to 'dover' to create MR with
    the manifest file of updated nvr.

    Parameters:
    nvr (str): nvr retrieved from manifest file
    upstream_nvr (str): incoming updated nvr
    fileName (str): JSON file name
    file_data (str): JSON file data
    endpoint (str): dover's endpoint to be sent request to

    Return:
    int: returning status code of the POST request
    """
    mp_encoder = MultipartEncoder(
        fields={
            "branch": f"update-package-{upstream_nvr}",
            "commitMessage": f"update from {nvr} -> {upstream_nvr}",
            "fileName": file_name,
            "data": file_data,
        }
    )

    try:
        return requests.post(
            app.urls[endpoint],
            data=mp_encoder,
            headers={"Content-Type": mp_encoder.content_type},
        ).status_code
    except requests.exceptions.HTTPError as e:
        logger.exception(e)
        return 0


def get_upstream_manifest(name: str) -> str:
    manifest_data: Any = ""
    url = app.urls[name]

    try:
        response = requests.get(url, allow_redirects=True, timeout=(5, 15))
        manifest_data = json.loads(response.text)
    except requests.exceptions.RequestException as e:
        logger.exception(f"Error in getting the file:{url}, error: {e}")
    except JSONDecodeError as e:
        logger.exception(f"Error in decoding JSON file, error: {e}")

    return manifest_data


def format_message(message: fedora_messaging) -> dict:
    m = Message()
    m.stream_message(message)
    return m.get_labels()
