from typing import Any

CS8: str = "cs8"
CS9: str = "cs9"
COMMON: str = "common"
ARCH: str = "arch"
AARCH64: str = "aarch64"
X86_64: str = "x86_64"
schema = dict[dict[dict[dict[str, Any]]]]


class PackageManifestSchema:
    """
    PackageManifestSchema is a helper class which
    is used to serialize and deserialise json data with
    the set schema
    """

    def __init__(self):
        """
        Contructor for the PackageManifestSchema class
        Default constructs
            - common
            - aarch64
            - x86_64
        """
        self.common: list = []
        self.aarch64: list = []
        self.x86_64: list = []

    def deserialize(
        self, package_manifest_obj: schema, centos_version: str = CS9
    ):
        """
        deserialize the JSON object to python equivalent objects, specifically
        in the lists of the PackageManifestSchema class attributes

        Parameters
        ----------
        package_manifest_obj: schema, python equilvalent of the json data
        centos_version: str, centos stream version

        """
        self.common = package_manifest_obj[centos_version][COMMON]
        self.aarch64 = package_manifest_obj[centos_version][ARCH][AARCH64]
        self.x86_64 = package_manifest_obj[centos_version][ARCH][X86_64]

    def serialize(self, centos_version: str = CS9) -> schema:
        """
        serialize the python data to a json equivalent object
        Parameters
        ----------
        centos_version: str, centos stream version defaulted to cs9

        """
        return {
            centos_version: {
                COMMON: self.common,
                ARCH: {
                    X86_64: self.x86_64,
                    AARCH64: self.aarch64,
                },
            }
        }
