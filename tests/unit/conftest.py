import pytest


@pytest.fixture
def nvrs() -> list:
    return [
        "libsmbclient-4.14.5-2.el8",
        "ModemManager-1.10.8-3.el8",
        "ModemManager-glib-1.10.8-3.el8",
    ]


@pytest.fixture
def submit_package_vars() -> list:
    return [
        "libsmbclient-4.14.5-2.el8",
        "libsmbclient-4.15.5-2.el8",
        "dover_submit_endpoint",
    ]


@pytest.fixture
def json_file_data() -> str:
    return '{"cs9":{"common":["abattis-cantarell-fonts-0.0.25-6.el8"],\
"arch":{"x86_64":["aspell-0.60.6.1-22.el8"],\
"aarch64":["grub2-efi-aa64-2.02-106.el8"]}}}'


@pytest.fixture
def message_body() -> str:
    return '"body": { \
                    "artifact": { \
                        "component": "abattis-cantarell-fonts", \
                        "scratch": "false", \
                        "id": "68771870", \
                        "nvr": "abattis-cantarell-fonts-1.0.25-6.el8", \
                        "type": "koji-build", \
                        "issuer": "deamn"} \
                    } \
            "topic": "test"'
