FROM registry.fedoraproject.org/fedora:34

RUN dnf -y install fedora-messaging \
    python3-pip \
    make \
    poetry \
    &&  mkdir /home/rome

WORKDIR /home/rome

COPY . /home/rome

RUN poetry config virtualenvs.create false \
    && poetry install \
    && rm -rf ~/.cache

ENTRYPOINT poetry run python3 ./rome/main.py
